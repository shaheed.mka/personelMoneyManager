/**
 * @author shahid
 */
package com.shaheed.pmm.constant;

public class Constants {
	
	public static final int RESPONSE_SUCCESS = 0;
	public static final int RESPONSE_FAILURE = 1;
	
	public static final String SUCCESS_GET_PERSON = "Successfully Fetched Person Details";
	public static final String INVALID_PERSON_NAME = "Person Name cannot be Null/Empty";

}
