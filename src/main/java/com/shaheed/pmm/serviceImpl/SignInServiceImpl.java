/**
 * @author shaheed
 * */
package com.shaheed.pmm.serviceImpl;

import static com.shaheed.pmm.constant.Constants.INVALID_PERSON_NAME;
import static com.shaheed.pmm.constant.Constants.SUCCESS_GET_PERSON;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shaheed.pmm.model.PersonModel;
import com.shaheed.pmm.repository.PersonModelRepository;
import com.shaheed.pmm.service.SignInService;
import com.shaheed.pmm.util.PMMUtility;

import lombok.extern.slf4j.Slf4j;

/**
 * @see com.shaheed.pmm.web.service.SignInService(com.shaheed.pmm.request.BasicRequest
 *      request)
 */
@Service
@Slf4j
public class SignInServiceImpl implements SignInService {

	@Autowired
	private PersonModelRepository personModelRepository;

	@Override
	public boolean validateUser(String personName) {
		log.info("In validateUser()");
		if (StringUtils.isNotBlank(personName)) {
			Optional<PersonModel> person = getPersonDetails(personName);
			boolean isPresent = person.isPresent();
			log.info("Retrieved person is present {}", isPresent);
			return isPresent;
		}
		return false;
	}

	@Override
	public boolean validatePassword(String userName, String userPas) {
		log.info("In validatePassword()");
		if (StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(userPas)) {
			Optional<PersonModel> person = getPersonDetails(userName);
			boolean isPresent = person.isPresent();
			log.info("Retrieved person is present {}", isPresent);
			if (isPresent) {
				boolean verified = PMMUtility.matchPassword(userPas, person.get().getPassword());
				if (verified) {
					log.info("Password verification is success for User {}", userName);
				} else {
					log.info("Password verification failed for User {}. Wrong Password.", userName);
				}
				return verified;
			} else {
				log.info("Password verification failed. User {} doesnot exists", userName);
			}
		} else {
			log.info("Password verification failed for User {} due to invalid credentials.", userName);
		}

		return false;
	}

	@Override
	public Optional<PersonModel> getPersonDetails(String personName) {
		log.info("In getUserDetails()");
		Optional<PersonModel> person = Optional.empty();
		if (StringUtils.isNotBlank(personName)) {
			try {
				person = personModelRepository.findByUserNameIgnoreCase(personName);
				log.info(SUCCESS_GET_PERSON);
			} catch (Exception exc) {
				log.error("Exception While Fetching User {}", exc);
			}
		} else {
			log.info(INVALID_PERSON_NAME);
		}
		return person;
	}

}
