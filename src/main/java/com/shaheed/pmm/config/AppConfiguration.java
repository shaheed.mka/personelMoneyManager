package com.shaheed.pmm.config;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.shaheed.pmm.model.PersonModel;
import com.shaheed.pmm.response.Response;
import com.shaheed.pmm.util.PMMUtility;

@Configuration
@EnableTransactionManagement
public class AppConfiguration {

	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Response getResponse() {
		return new Response();
	}

	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public PersonModel getPersonModel() {
		return new PersonModel();
	}

	@Bean
	public PMMUtility getPMMUtility() {
		return new PMMUtility();
	}

}
