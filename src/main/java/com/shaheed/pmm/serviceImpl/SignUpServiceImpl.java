package com.shaheed.pmm.serviceImpl;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shaheed.pmm.model.PersonModel;
import com.shaheed.pmm.repository.PersonModelRepository;
import com.shaheed.pmm.service.SignUpService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SignUpServiceImpl implements SignUpService {

	@Autowired
	private PersonModelRepository personModelRepository;

	@Override
	public boolean createUser(PersonModel personModel) {
		log.info("In createUser()");
		if (personModel != null && StringUtils.isNotBlank(personModel.getUserName())) {
			Optional<PersonModel> person = Optional.of(personModelRepository.save(personModel));
			boolean isCreated = person.isPresent();
			if (isCreated) {
				log.info("Successfully saved in DB {}", person.get());
				return true;
			}
		}
		log.info("Not saved in DB");
		return false;
	}

}
