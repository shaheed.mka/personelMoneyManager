/**
 * @author shahid
 */
package com.shaheed.pmm.controller;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.shaheed.pmm.constant.Constants;
import com.shaheed.pmm.model.PersonModel;
import com.shaheed.pmm.response.Response;
import com.shaheed.pmm.service.SignInService;
import com.shaheed.pmm.service.SignUpService;
import com.shaheed.pmm.util.PMMUtility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/personelMoneyManager/signup")
@Api(value = "Personel Money Manager SignUp")
@Slf4j
public class SignUpController {

	@Autowired
	Response response;

	@Autowired
	SignUpService signUpService;

	@Autowired
	SignInService signInService;

	@RequestMapping(value = "/createUser", method = RequestMethod.POST, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 0, message = "Successful"),
			@ApiResponse(code = 1, message = "Internal Server Error") })
	public Response createUser(@RequestBody PersonModel personModel) throws JsonProcessingException {
		log.info("UserName Request {}", personModel.toString());
		Optional<PersonModel> person = signInService.getPersonDetails(personModel.getUserName());
		if (!person.isPresent()) {
			personModel.setPassword(PMMUtility.encode(personModel.getPassword()));
			personModel.setCreatedDate(LocalDateTime.now());
			personModel.setModifiedDate(personModel.getCreatedDate());
			personModel.setIsActive(true);
			boolean isCreated = signUpService.createUser(personModel);

			if (isCreated) {
				response.setStatusCode(Constants.RESPONSE_SUCCESS);
				response.setStatusDescription("User created Successfully");
				response.setResponseObject(null);
				log.info("SignUp success");
			} else {
				response.setStatusCode(Constants.RESPONSE_FAILURE);
				response.setStatusDescription("User creation failed");
				response.setResponseObject(null);
				log.info("SignUp failed");
			}
		} else {
			response.setStatusCode(Constants.RESPONSE_SUCCESS);
			response.setStatusDescription("UserName Exists. Please try with different userName");
			response.setResponseObject(null);
			log.info("UserName Exists");
		}
		log.info("Response {}", response.toString());
		return response;

	}

}
