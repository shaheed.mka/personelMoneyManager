package com.shaheed.pmm.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.shaheed.pmm.model.PersonModel;

public class PMMUtility {

	@Autowired
	PersonModel personModel;

	public static String encode(String value) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(5);
		return encoder.encode(value);
	}

	public static boolean matchPassword(String rawPas, String encodedPas) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(5);
		return encoder.matches(rawPas, encodedPas);
	}

}
