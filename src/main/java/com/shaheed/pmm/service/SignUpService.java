/**
 * @author shahid
 * */
package com.shaheed.pmm.service;

import com.shaheed.pmm.model.PersonModel;

public interface SignUpService {
	
	/**
	 * Service to create user
	 * @param personModel
	 * @return
	 * */
	public boolean createUser(PersonModel personModel);

}
