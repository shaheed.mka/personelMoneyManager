/**
 * @author shaheed
 * */
package com.shaheed.pmm.service;

import java.util.Optional;

import com.shaheed.pmm.model.PersonModel;

public interface SignInService {

	/**
	 * Service to authenticate user
	 * @param basicRequest
	 * @return
	 * */
	public boolean validateUser(String userName);
	
	/**
	 * Service to authenticate password
	 * @param passwordRequest
	 * @return
	 * */
	public boolean validatePassword(String userName, String userPas);
	
	/**
	 * Service to fetch user details
	 * @param personName
	 * @return
	 * */
	public Optional<PersonModel> getPersonDetails(String personName);
}
