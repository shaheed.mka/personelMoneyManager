/**
 * @author shaheed
 * Controller for SignIn and SignUp forms*/
package com.shaheed.pmm.controller;

import static com.shaheed.pmm.constant.Constants.SUCCESS_GET_PERSON;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.shaheed.pmm.constant.Constants;
import com.shaheed.pmm.model.PersonModel;
import com.shaheed.pmm.response.Response;
import com.shaheed.pmm.service.SignInService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/personelMoneyManager/signin")
@Api(value = "Personel Money Manager SignIn")
@Slf4j
public class SignInController {

	@Autowired
	SignInService signInService;

	@Autowired
	Response response;

	@RequestMapping(value = "/validateUserName", method = RequestMethod.POST, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 0, message = "Successful"),
			@ApiResponse(code = 1, message = "Internal Server Error") })
	public Response validateUserName(
			@ApiParam(value = "A valid username.", required = true) @RequestParam String userName) throws Exception {
		log.info("UserName Request {}", userName);
		boolean isValidUser = signInService.validateUser(userName);
		if (isValidUser) {
			response.setStatusCode(Constants.RESPONSE_SUCCESS);
			response.setStatusDescription("User verification Success");
			response.setResponseObject(null);
			return response;

		} else {

			response.setStatusCode(Constants.RESPONSE_FAILURE);
			response.setStatusDescription("Failed to verify user");
			response.setResponseObject(null);
			return response;
		}

	}

	@RequestMapping(value = "/validatePassword", method = RequestMethod.POST, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 0, message = "Successful"),
			@ApiResponse(code = 1, message = "Internal Server Error") })
	public Response validateUserName(
			@ApiParam(value = "A valid username.", required = true) @RequestParam String userName,
			@ApiParam(value = "A valid password.", required = true) @RequestParam String userPas) throws Exception {
		log.info("UserName, Password Request {},{}", userName, userPas);
		boolean isValidUser = signInService.validatePassword(userName, userPas);
		if (isValidUser) {
			response.setStatusCode(Constants.RESPONSE_SUCCESS);
			response.setStatusDescription("Password verification Success");
			response.setResponseObject(null);
			return response;

		} else {

			response.setStatusCode(Constants.RESPONSE_FAILURE);
			response.setStatusDescription("Failed to verify Password");
			response.setResponseObject(null);
			return response;
		}

	}

	@RequestMapping(value = "/getUserDetails", method = RequestMethod.GET, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 0, message = "Successful"),
			@ApiResponse(code = 1, message = "Internal Server Error") })
	public Response getPersonDetails(
			@ApiParam(value = "A valid username.", required = true) @RequestParam String userName)
			throws JsonGenerationException, JsonMappingException, IOException {
		log.info("In getPersonDetails() controller");
		Optional<PersonModel> person = signInService.getPersonDetails(userName);
		if (person.isPresent()) {
			response.setStatusCode(Constants.RESPONSE_SUCCESS);
			response.setResponseObject(person.get());
			response.setStatusDescription(SUCCESS_GET_PERSON);
			log.info("Success response is {}", response.toString());
		} else {
			response.setStatusCode(Constants.RESPONSE_SUCCESS);
			response.setStatusDescription("No Records Found with User Name : " + userName);
			response.setResponseObject(null);
			log.info("No Records Found with User Name : " + userName);
		}
		return response;

	}

}
