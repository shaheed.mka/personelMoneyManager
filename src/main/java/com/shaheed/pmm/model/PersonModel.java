/**
 * @author shaheed
 * */
package com.shaheed.pmm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Setter
@Getter
@Table(name = "person")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder({ "personId", "userName", "personFirstName", "personLastName", "emailId", "mobileNumber", "address",
		"isActive" })
@JsonIgnoreProperties({ "password", "createdDate", "modifiedDate" })
public class PersonModel extends CommonDatesModel {

	/**
	 * Generated Serial Version UID
	 **/
	private static final long serialVersionUID = -4406091364609908184L;

	@GeneratedValue(generator = "person_person_id_seq")
	@SequenceGenerator(name = "person_person_id_seq", sequenceName = "person_person_id_seq", initialValue = 1000)
	@Id
	@Column(name = "personid", nullable = false, unique = true)
	@ApiModelProperty(hidden = true)
	private Long personId;

	@Column(name = "username", nullable = false, unique = true)
	@NotBlank
	@Size(min = 7)
	private String userName;

	@Column(name = "person_first_name", nullable = false)
	@NotBlank
	private String personFirstName;

	@Column(name = "person_last_name")
	private String personLastName;

	@Column(name = "email_id", nullable = false, unique = true)
	@NotBlank
	private String emailId;

	@Column(name = "mobile_number", nullable = false, unique = true)
	@NotBlank
	@Size(min = 10, max = 14)
	private String mobileNumber;

	@Column(name = "address")
	private String address;

	@Column(name = "is_active")
	@ApiModelProperty(hidden = true)
	private Boolean isActive;

	@Column(name = "password")
	private String password;

	@Override
	public String toString() {
		return " person: \n { \n  personId:" + personId + ", \n  userName:" + userName + ", \n  personFirstName:"
				+ personFirstName + ", \n  personLastName:" + personLastName + ", \n  emailId:" + emailId
				+ ", \n  mobileNumber:" + mobileNumber + ", \n  address:" + address + ", \n  createdDate:" + createdDate
				+ ", \n  modifiedDate:" + modifiedDate + ", \n  isActive:" + isActive + ", \n  password:" + password
				+ "\n }";
	}
}
