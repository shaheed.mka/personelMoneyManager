package com.shaheed.pmm.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Response implements Serializable {

	/**
	 * Generated serial version Id
	 */
	private static final long serialVersionUID = -6621226902942428838L;

	@Override
	public String toString() {

		return "response \n { \n statusCode:" + statusCode + ", \n statusDescription:" + statusDescription + ", \n"
				+ responseObject + "\n}";
	}

	@Setter
	@Getter
	private int statusCode;

	@Setter
	@Getter
	private String statusDescription;

	@Setter
	@Getter
	private Object responseObject;

}
