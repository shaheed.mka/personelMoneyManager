/**
 * @author shaheed
 **/
package com.shaheed.pmm.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdDate", "modifiedDate" }, allowGetters = true)
@Getter
@Setter
public abstract class CommonDatesModel implements Serializable {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 4497811132376892117L;

	@Basic
	@Column(name = "created_date", nullable = false, updatable = false)
	@CreatedDate
	@ApiModelProperty(hidden = true)
	protected LocalDateTime createdDate;

	@Basic
	@Column(name = "modified_date", nullable = false)
	@LastModifiedDate
	@ApiModelProperty(hidden = true)
	protected LocalDateTime modifiedDate;

	@Override
	public String toString() {
		return "CommonDatesModel: \n { \n createdDate:" + createdDate + ", \n  modifiedDate:" + modifiedDate + "\n}";
	}

}
